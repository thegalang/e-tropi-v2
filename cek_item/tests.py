from django.test import TestCase, Client
from django.urls import resolve
from .models import Data, CelebrityPhoto
import json
from .views import post_data
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from pyvirtualdisplay import Display
from django.contrib import auth
from django.contrib.auth.models import User
import json


# mengetes general
class CekItemUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def test_landing_redirects(self):
        response = self.client.get('')
        self.assertRedirects(response, '/cek_item/')

    def test_exist_page(self):
        response = self.client.get('/cek_item/')
        self.assertEquals(response.status_code, 200)

    def test_model_saving_works(self):
        new_data = Data(objectphoto=self.test_img_link, genuine=True)

        self.assertEquals(Data.objects.count(), 0)

        new_data.save()

        self.assertEquals(Data.objects.count(), 1)
        self.assertIsInstance(new_data, Data)

    def test_model_saving_last_24(self):
        for i in range(30):
            new_data = Data(objectphoto=self.test_img_link, genuine=True)
            new_data.save()

        self.assertEquals(Data.objects.count(), 24)

        # 6 - 29 ada, 0 - 5 gaada
        self.assertEquals(Data.objects.filter(id=30).count(), 1)
        self.assertEquals(Data.objects.filter(id=7).count(), 1)
        self.assertEquals(Data.objects.filter(id=6).count(), 0)

    def test_form_saving_works(self):
        self.assertEquals(Data.objects.count(), 0)
        response = self.client.post('/cek_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        time.sleep(4)
        self.assertEquals(Data.objects.count(), 1)


# mengetes berhubungan dengan models di unittest
class CekItemModelUnitTest(TestCase):

    def setUp(self):
        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

        # ramein celebrity
        for i in range(10):
            new_celeb = CelebrityPhoto(name=str(i), photo=self.test_img_link)
            new_celeb.save()

    # cek apakah automatis dikasih 8
    def test_form_giving_8_celebs(self):

        response = self.client.post('/cek_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(Data.objects.all()[0].celebrities.count(), 8)

    def test_random_assignment_works(self):

        genuines = set({})

        # post banyak kali. diharapkan ada yang asli dan ada yang palsu
        for i in range(10):
            response = self.client.post('/cek_item/post_data/',
                                        data={'data': self.test_img_link},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            genuines.add(Data.objects.all()[i].genuine)

        self.assertEquals(len(genuines), 2)

    def test_history_is_given(self):

        # post banyak untuk ngisi history
        for i in range(3):
            response = self.client.post('/cek_item/post_data/',
                                        data={'data': self.test_img_link},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        response = self.client.get('/cek_item/get_history_data/',
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        json_data = response.json()

        self.assertEquals(len(json_data), 3)

    def test_query_model_works(self):

        test_cases = ["https://files.catbox.moe/rejga0.jpg",
                      "https://files.catbox.moe/4vdm8q.jpg"]

        # mengisi database data
        for test_case in test_cases:
            response = self.client.post('/cek_item/post_data/',
                                        data={'data': test_case},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # coba query tiap data dan cek apakah sama
        for id, test_case in enumerate(test_cases):
            response = self.client.get('/cek_item/get_model_data/?obj_id=' + str(id + 1),
                                       content_type='application/json',
                                       HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            json_data = response.json()[0]
            self.assertEquals(json_data["objectphoto"], test_case)

    def test_sending_invalid_link_rejects(self):

        self.assertEquals(Data.objects.count(), 0)

        test_cases = ["https://catbox.moe",
                      "https://www.facebook.com",
                      "https://codeforces.com"]

        for test_case in test_cases:
            response = self.client.post('/cek_item/post_data/',
                                        data={'data': test_case},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(Data.objects.count(), 0)


# mengetest berhubungan dengan authentication di cek item
class CekItemAuthUnitTest(TestCase):

    def setUp(self):
        response = self.client.post('/profile/register/', data={"username": "galatea",
                                                                "password1": "orcust_babel",
                                                                "password2": "orcust_babel"})
        self.test_user = User.objects.get(username="galatea")

        response = self.client.post('/profile/login/', data={"username": "galatea",
                                                             "password": "orcust_babel"}, follow=True)
        # cek sudah kelogin belum
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)

        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    # cek apakah history dia sudah ada
    def test_posting_data_registered_in_history(self):
        self.assertEquals(self.test_user.userhistory_set.count(), 0)

        request = self.client.post('/cek_item/post_data/',
                                   data={'data': self.test_img_link},
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        # cek apakah terdaftar
        self.assertEquals(self.test_user.userhistory_set.count(), 1)
        self.assertEquals(self.test_user.userhistory_set.all()[0].objectphoto, self.test_img_link)


class CekItemFunctionalTest(TestCase):

    # set-up and teardown from my story-6
    def setUp(self):

        # global display
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        service_log_path = './chromedriver.log'
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def tearDown(self):

        self.display.stop()
        self.browser.quit()

    # ngecek apakah genuine display celeb, counterfeit tidak display celeb
    def test_submit_displays_property_celeb(self):

        self.browser.get('localhost:8000')
        time.sleep(1)

        form_field = self.browser.find_element_by_id('id_data')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        celebs_display = self.browser.find_element_by_id('celebs')
        form_field.send_keys(self.test_img_link)
        time.sleep(1.5)

        genuine_done = False
        counterfeit_done = False

        # submit terus sampai dapat genuine
        while (True):

            time.sleep(0.5)
            submit_button.click()
            time.sleep(6)

            # ulang terus sampai pernah dapat genuine & counterfeit
            if label_status.text == "Counterfeit":

                self.assertFalse(celebs_display.is_displayed())
                counterfeit_done = True

            else:

                self.assertTrue(celebs_display.is_displayed())
                genuine_done = True

            if counterfeit_done and genuine_done:
                break

    # test apakah history dapat di klik. sekalin test loading
    def test_history_is_displaying(self):

        self.browser.get('localhost:8000')
        time.sleep(3)

        form_field = self.browser.find_element_by_id('id_data')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        loading_gif = self.browser.find_element_by_id('loading_content')
        img_links = {"https://files.catbox.moe/rejga0.jpg",
                     "https://files.catbox.moe/4vdm8q.jpg"}

        # posting 2 benda
        for link in img_links:
            form_field.clear()
            form_field.send_keys(link)

            time.sleep(2)
            submit_button.click()
            self.assertTrue(loading_gif.is_displayed())
            time.sleep(6)
            self.assertFalse(loading_gif.is_displayed())

        time.sleep(5)

        # coba klik satu-satu
        hist_photos = self.browser.find_elements_by_class_name('history_photo')[-2:]
        found_links = set({})
        for photos in hist_photos:
            time.sleep(2)
            photos.click()
            time.sleep(6)
            found_links.add(self.browser.find_element_by_id('image').get_attribute('src'))

        # kalau fotonya beda semua, maka history bekerja
        self.assertEquals(len(found_links), 2)
