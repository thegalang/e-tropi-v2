from django.contrib import admin 
from . import views
from django.urls import path, include

app_name = "cek_item"

urlpatterns = [
    path('', views.index, name='home'),
    path('post_data/', views.post_data, name='post_data'),
    path('get_history_data/', views.get_history_data, name='get_history_data'),
    path('get_model_data/', views.get_model_data, name='get_model_data'),
]