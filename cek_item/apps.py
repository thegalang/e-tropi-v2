from django.apps import AppConfig # pragma: no cover


class CekItemConfig(AppConfig): # pragma: no cover
    name = 'cek_item'
