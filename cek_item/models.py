from django.db import models
from rest_framework import serializers


# hanya menyimpan 24 elemen terakhir
def keep_last_24(MyModel):
    most_recents = MyModel.objects.all()
    if (len(most_recents) > 24):
        last24 = MyModel.objects.all()[len(most_recents) - 24:]
        MyModel.objects.exclude(id__in=last24).delete()


class CelebrityPhoto(models.Model):
    name = models.TextField(max_length=20, default="", primary_key=True)
    photo = models.TextField(default="")

    class meta:
        verbose_name_plural = "celebrity photos"


class Data(models.Model):
    celebrities = models.ManyToManyField(CelebrityPhoto)
    objectphoto = models.TextField(default="")
    genuine = models.BooleanField(default="")

    class meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)

        keep_last_24(Data)


# class untuk serialize secara rekursif. 
# sumber: https://ruddra.com/posts/django-serialize-foreignkey-m2m-property-instance-fields/ 
class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = "__all__"
        depth = 1
