from django.shortcuts import render, redirect
from .models import Data, CelebrityPhoto, DataSerializer
from .forms import DataForm
from django.http import JsonResponse
import random
import json
import time
from django.contrib.auth.models import User
import requests
from PIL import Image
from io import BytesIO


# import http.client as http
def landing_page(request):
    return redirect('cek_item:home')


# pilih 8 foto random untuk setiap objek
def populate_celebs(data):
    selected = CelebrityPhoto.objects.order_by('?')[:8]
    for celeb in selected:
        data.celebrities.add(celeb)


# cek apakah link image valid
# sumber: https://stackoverflow.com/questions/11261210/django-check-if-an-image-exists-at-some-particular-url
def valid_image_link(link):
    try:

        response = requests.get(link)
        img = Image.open(BytesIO(response.content))
        img.verify()
        return True

    except:
        return False


def post_data(request):
    if request.is_ajax() and request.method == 'POST':

        json_data = json.loads(request.body)
        objectphoto = json_data['data']

        # ngecek kevalalidan link image
        if (not valid_image_link(objectphoto)):
            response = JsonResponse({'message': "Invalid image link!"})
            response.status_code = 400
            return response

        # menentukan apakah barang asli atau palsu secara random (50%)
        genuine = True
        num = random.random()
        if (num > 0.5):
            genuine = False

        data = Data(genuine=genuine,
                    objectphoto=json_data['data'])

        data.save()  # harus di save kalau mau masukin ManytoMany
        populate_celebs(data)
        data.save()

        # simpan data ini ke history jika usernya login
        if request.user.is_authenticated:
            current_user = User.objects.get(username=request.user.username)
            new_history = current_user.userhistory_set.create(operation_name="Check Item",
                                                              objectphoto=json_data['data'])

        # ubah data ini ke json lalu balikin
        serializer = DataSerializer(data)
        serialized = serializer.data

        return JsonResponse(serialized, safe=False)


def get_history_data(request):
    if request.is_ajax():
        serializer = DataSerializer(Data.objects.all(), many=True)
        serialized = serializer.data

        time.sleep(2)

        return JsonResponse(serialized, safe=False)


def get_model_data(request):
    if request.is_ajax():
        obj_id = request.GET.get('obj_id', '')

        # cari model dengan id obj_id
        if (obj_id != ''):
            queried = Data.objects.filter(id=obj_id)
            serializer = DataSerializer(queried, many=True)
            time.sleep(2)

            return JsonResponse(serializer.data, safe=False)


def index(request):
    context = {'app': 'cek_item'}
    form = DataForm()
    context['form'] = form
    return render(request, 'cek_item/index.html', context)
