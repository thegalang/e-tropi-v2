$(document).ready(function () {

    $("#submit_form").submit(function (event) {

        $("#button").prop('disabled', true);

        event.preventDefault();

        // dapetin form & datanya
        var $form = $(this), send_to = $form.attr('action')
        var csrf = $('[name="csrfmiddlewaretoken"]').val()
        var formData = {
            data: $('#id_data').val(),

        };
        console.log(formData)
        nformdata = JSON.stringify(formData);


        $.ajax({
            type: "post",
            url: send_to,
            dataType: 'json',
            contentType: 'application/json; charset=utf-8',
            headers: {
                'Content-Type': 'application/json',
                'X-CSRFToken': csrf,
            },
            data: nformdata,
            beforeSend: before_send_function(),
            success: function (data_returned, textStatus, jqXHR) {

                edit_content(data_returned);
                load_history("/cek_peta/get_history_data/");
                $("#button").prop('disabled', false);

            },
            error: function(data_returned) {

				$("#loading_content").css('display', "none");
				alert(data_returned.responseJSON.message);
				$("#button").prop("disabled", false);
			}
        });

    });
});

// fungsi yang dipanggil sebelum update
function before_send_function() {

    // hapus & sembunyiin
    $("#content").css("display", "none");
    $("#bag-img").attr('src', '');
    $("#map").attr("src", '');
    $("#city").attr("src", '');
    $("#negara").empty();
    $("#kota").empty();

    // munculin loading
    $("#loading_content").css("display", "block");
}


// edit data dari sebuah instansi model ke bagian content
function edit_content(data) {

    $("#loading_content").css("display", "none");
    console.log(data);

    // update deskripsi barang
    $("#bag-img").attr('src', data.objectphoto);
    $("#map").attr('src', data.map.mapPict);
    $("#city").attr('src', data.map.cityPict);
    console.log(data.objectphoto)

    $("#content").slideDown();

    // menambahkan peta
    $("#negara").text("indonesia");
    $("#kota").text("jakarta");

}