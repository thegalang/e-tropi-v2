$(document).ready(function() {

	$("#submit_form").submit(function(event) {

		$("#submit_button").prop('disabled', true);

		event.preventDefault();

		// dapetin form & datanya
		var $form = $( this ), send_to = $form.attr('action')
		var csrf = $('[name="csrfmiddlewaretoken"]').val()
		var formData = {data: $('#id_data').val(),
						
						};

		nformdata = JSON.stringify(formData);
		

		$.ajax({
			type: "post",
			url: send_to,
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			headers: {
                    'Content-Type':'application/json',
                    'X-CSRFToken': csrf,
            },
			data: nformdata, 
			beforeSend: before_send_function(),
			success: function(data_returned, textStatus, jqXHR) {

				edit_content(data_returned);
				load_history("/about_item/get_history_data/");
				$("#submit_button").prop('disabled', false);

			},
			error: function(data_returned, textStatus, jqXHR) {

				$("#loading_content").css('display', "none");
				alert(data_returned.responseJSON.message);
				$("#submit_button").prop("disabled", false);
			}
		});

	});
});