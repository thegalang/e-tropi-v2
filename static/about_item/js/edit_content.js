// edit data dari sebuah instansi model ke bagian content
function edit_content(data) {

	// update deskripsi barang
	$("#image").attr("src", data.objectphoto);
	$("#material_data").text(data.material);
	$("#price_data").text(data.price);
	$("#year_data").text(data.year);

	// menampilakan semuanya
	$("#loading_content").css("display", "none");
	$("#content").css("display", "block");
}

// fungsi yang dipanggil sebelum update
function before_send_function() {

	$("#content").css("display", "none");
	$("#image").attr("src", "");
	$("#material_data").text("");
	$("#price_data").text("");
	$("#year_data").text("");

	// munculin loading
	$("#loading_content").css("display", "block");
}

