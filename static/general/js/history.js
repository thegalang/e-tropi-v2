function load_history(from) {

	$.ajax({
		type: "get",
		url: from,
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		headers: {
                'Content-Type':'application/json',
        },
		beforeSend: function() {

			// hapus sembunyiin
			$("#history_content").empty();
			$("#history_content").css("display", "none");
		
			// munculin loading
			$("#loading_history").css("display", "block");
		},
		success: function(data_returned, textStatus, jqXHR) {

			// sembunyiin loading dan tunjukkan konten
			edit_history(data_returned)
			$("#loading_history").css("display", "none");
			$("#history_content").slideDown();


		},
		error: function(jqXHR, textStatus, errorThrown) {

		}
	});
}

function edit_history(datas) {

	// mengisi history
	$.each(datas, function(index, item) {

		$("#history_content").append(
			'<div class="col-4 col-md-2 mb-4">' +
			'<img class="history_photo img-fluid" src="' +item.objectphoto+'" obj_id="'+item.id+'" alt="history_object" />' +
			'</div>' 
		)
	});
}

$(document).on('click','.history_photo',function(){

	// buat border di yang di klik
	$(".history_photo").css("border", "none")
	$(this).css("border", "0.1em solid #afd275")

	// dapetin data object ini
	var obj_id = $(this).attr('obj_id');
 	var from = $("#model_get_from").attr('data')
 	var query_data = {obj_id : obj_id};

 	$.ajax({
		type: "get",
		url: from,
		dataType: 'json',
		contentType: 'application/json; charset=utf-8',
		headers: {
                'Content-Type':'application/json',
        },
        data: query_data,
        beforeSend: before_send_function(),
		success: function(data_returned, textStatus, jqXHR) {

			edit_content(data_returned[0]);
		},
		error: function(jqXHR, textStatus, errorThrown) {

		}
	});

});

$(document).on('mouseenter', '.history_photo', function() {
	$(this).css('cursor', "pointer");
});