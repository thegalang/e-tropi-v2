// edit data dari sebuah instansi model ke bagian content
function edit_content(data) {

	// update deskripsi barang
	$("#image").attr('src', data.objectphoto);
	if(data.genuine) {

		$("#genuine_desc").text("Genuine");

		$("#celebs").slideDown();
		// menambahkan celebrity
		$.each(data.celebrities, function(index, item) {
			$("#celeb_photos").append(
				'<div class="col-6 col-md-3 mb-4">' + 
                '<img class="celebrity_photo .img-fluid" src= "' + item.photo + '" title = "' + item.name + '" >' +
            	'</div>'
			);
		});
	}
	else {
		$("#genuine_desc").text("Counterfeit");
	}

	// menampilakan semuanya
	$("#loading_content").css("display", "none")
	$("#content").slideDown();

}

// fungsi yang dipanggil sebelum update
function before_send_function() {

	// hapus & sembunyiin
	$("#celebs").css("display", "none")
	$("#content").css("display", "none");
	$("#image").attr('src', '')
	$("#celeb_photos").empty();

	// munculin loading
	$("#loading_content").css("display", "block");
}

