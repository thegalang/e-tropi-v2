$(document).ready(function() {

	$("#submit_form").submit(function(event) {

		$("#submit_button").prop('disabled', true);

		event.preventDefault();

		// dapetin form & datanya
		var $form = $( this ), send_to = $form.attr('action')
		var csrf = $('[name="csrfmiddlewaretoken"]').val()
		var formData = {data: $('#id_data').val(),
						
						};

		nformdata = JSON.stringify(formData);
		

		$.ajax({
			type: "post",
			url: send_to,
			dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			headers: {
                    'Content-Type':'application/json',
                    'X-CSRFToken': csrf,
            },
			data: nformdata, 
			beforeSend: before_send_function(),
			success: function(data_returned, textStatus, jqXHR) {

				edit_content(data_returned);
				load_history("/cek_item/get_history_data/");
				$("#submit_button").prop('disabled', false);

			},
			error: function(data_returned) {

				$("#loading_content").css('display', "none");

				//console.log(data_returned.promise);
				//console.log(data_returned);
				alert(data_returned.responseJSON.message);
				// console.log(jqXHR);
				// console.log(textStatus);
				// console.log(errorThrown);
				$("#submit_button").prop("disabled", false);
			}
		});

	});
});

$(document).on('mouseenter','.celebrity_photo',function(){
	 $(this).css("cursor","pointer");
	 $(this).animate({'width': "12.5em", "height": "12.5em"}, 'medium');
});

$(document).on('mouseleave','.celebrity_photo',function(){
	 $(this).animate({'width': "10em", "height": "10em"}, 'medium');
});

$(document).on('click', '.celebrity_photo', function() {

	var celebrity_name = $(this).attr('title')
	window.open("https://www.google.com/search?q="+celebrity_name)
})