$(document).ready(function() {

	$("#submit_form").submit(function(event) {

		$("#submit_button").prop('disabled', true);

		event.preventDefault();

		// dapetin form & datanya
		var $form = $( this ), send_to = $form.attr('action')
		var csrf = $('[name="csrfmiddlewaretoken"]').val()
		var formData = {data: $('#id_ObjectPhoto').val(),};

		nformdata = JSON.stringify(formData);
		

		$.ajax({
			type: "post",
			url: send_to,
		 	dataType: 'json',
			contentType: 'application/json; charset=utf-8',
			headers: {
                    'Content-Type':'application/json',
                    'X-CSRFToken': csrf,
            },
			data: nformdata, 
			beforeSend: before_send_function(),
			success: function(data_returned, textStatus, jqXHR) {

				edit_content(data_returned);
				load_history("/compare_item/get_history_data/");
				$("#submit_button").prop('disabled', false);

			},
			error: function(data_returned) {
				$("#loading_content").css('display', "none");
				alert(data_returned.responseJSON.message);
				$("#submit_button").prop("disabled", false);
			}
		});

	});
});

// edit data dari sebuah instansi model ke bagian content
function edit_content(data) {

	// update deskripsi barang
	$("#img1").attr('src', data.objectphoto);
	$("#img2").attr('src', data.objectphoto);
	if(data.Genuine) {

		$("#genuine_desc").text("Genuine");

		$("div.details").slideDown();

		$("#brand").text(data.Brand);
		$("#price").text("$ "+data.Price);
		$("#date").text(data.Release_date);
		$("#location").text(data.Brand + ", " + data.Location);

	}
	else {
		$("#genuine_desc").text("Counterfeit");
	}

	// menampilakan semuanya
	$("#loading_content").css("display", "none")
	$("#content").slideDown();

}

// fungsi yang dipanggil sebelum update
function before_send_function() {

	// hapus & sembunyiin
	$("#content").css("display", "none");
	$("#image").attr('src', '')
	$("div.details").css("display","none");

	// munculin loading
	$("#loading_content").css("display", "block");
}

$(document).on('mouseenter','.show-item div p',function(){
	 $(this).css({"cursor":"pointer", "background-color":"#b9bdba"});
});

$(document).on('mouseleave','.show-item div p',function(){
	$(this).css({"background-color":"#e3f2fd"});
});