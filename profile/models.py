from django.db import models
from django.contrib.auth.models import User

# simpan history dari 20 searches terakhir
def keep_last_20_history(user):

	most_recents = user.userhistory_set.all()
	if(len(most_recents)>20): # pragma: no cover
		last20 = user.userhistory_set.all()[len(most_recents) - 20:]
		user.userhistory_set.exclude(id__in=last20).delete()

class UserHistory(models.Model):

	user = models.ForeignKey(User, on_delete=models.CASCADE)
	operation_name = models.CharField(max_length=20)
	objectphoto = models.TextField(default="")

	def save(self, *args, **kwargs):

		super(UserHistory, self).save(*args, **kwargs)

		keep_last_20_history(self.user)