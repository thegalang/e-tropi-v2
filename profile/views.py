from django.shortcuts import render, redirect
from .forms import CustomRegistrationForm
from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login

def register_user(request):

	# dapat request
	if request.method == "POST":

		form = CustomRegistrationForm(request.POST)
		if(form.is_valid()):
			user = form.save()
			login(request, user)
			return redirect("profile:history")
	else:
		form = CustomRegistrationForm()

	context = {"app" : "profile",
				"form" : form,
				}
	#print("MASHOK", form.errors)
	return render(request, "profile/register.html", context)

@login_required
def history(request):

	context = {"app" : "profile"}
	return render(request, "profile/history.html", context)