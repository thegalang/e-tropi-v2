from django.test import TestCase, Client
from django.urls import resolve
import json
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from pyvirtualdisplay import Display
from django.contrib.auth.models import User
from django.contrib import auth
from .models import UserHistory
# Create your tests here.
class RegisterUnitTests(TestCase):

	def setUp(self):
		self.client = Client()

	def test_login_page_exists(self):
		response = self.client.get('/profile/login/', )
		self.assertEquals(response.status_code, 200)

	def test_register_exists(self):
		response = self.client.get('/profile/register/')
		self.assertEquals(response.status_code,200)

	def test_register_works(self):

		self.assertEquals(User.objects.count(), 0)
		response = self.client.post('/profile/register/', data={"username" : "galatea", 
														   "password1" : "orcust_babel",
														   "password2" : "orcust_babel"})
		self.assertEquals(User.objects.count(), 1)
		self.assertEquals(User.objects.all()[0].username, "galatea")

	def test_register_not_works_not_match_password(self):

		self.assertEquals(User.objects.count(), 0)
		response = self.client.post('/profile/register/', data={"username" : "galatea", 
														   "password1" : "orcust_babel",
														   "password2" : "orcust_crescendo"}, follow=True)
		self.assertEquals(User.objects.count(), 0)
		self.assertContains(response, "The two password fields didn&#39;t match.", count=1, html=True)

	def test_register_not_works_too_weak_password(self):

		self.assertEquals(User.objects.count(), 0)
		response = self.client.post('/profile/register/', data={"username" : "galatea", 
														   "password1" : "123456",
														   "password2" : "123456"}, follow=True)
		self.assertEquals(User.objects.count(), 0)
		self.assertContains(response, "This password is too common.", count=1, html = True)

class LoginLogoutUnitTests(TestCase):

	def setUp(self):

		response = self.client.post('/profile/register/', data={"username" : "galatea", 
														   "password1" : "orcust_babel",
														   "password2" : "orcust_babel"})
		
		response = self.client.get('/profile/logout/')

	def test_login_logout_works(self):


		response = self.client.post('/profile/login/', data = {"username" : "galatea",
															   "password" : "orcust_babel"}, follow=True)
		
		user = auth.get_user(self.client)
		self.assertTrue(user.is_authenticated)

		response = self.client.get('/profile/logout/')

		user = auth.get_user(self.client)
		self.assertFalse(user.is_authenticated)

	def test_login_not_works(self):

		response = self.client.post('/profile/login/', data = {"username" : "galatea",
															   "password" : "orcustbabel"}, follow=True)
		
		user = auth.get_user(self.client)
		self.assertFalse(user.is_authenticated)

	# history hanya bisa diakses jika user log-in
	def test_history_page_redirects_on_not_authenticated(self):

		response = self.client.get('/profile/history/')

		self.assertRedirects(response, '/profile/login/?next=/profile/history/')


	def test_history_page_exist(self):

		response = self.client.post('/profile/login/', data = {"username" : "galatea",
															   "password" : "orcust_babel"}, follow=True)
		response = self.client.get('/profile/history/', follow=True)

		self.assertEquals(response.status_code, 200)
		self.assertContains(response, "Hello galatea!", count=1, html=False)


class HistoryUnitTests(TestCase):

	def setUp(self):

		response = self.client.post('/profile/register/', data={"username" : "galatea", 
														   "password1" : "orcust_babel",
														   "password2" : "orcust_babel"})
		self.test_user = User.objects.get(username="galatea")

		test_datas = [("cek item", "https://files.catbox.moe/rejga0.jpg"),
					 ("compare item", "https://files.catbox.moe/4vdm8q.jpg"),
					 ("location check", "https://files.catbox.moe/4vdm8q.jpg"),
					 ("about item", "https://files.catbox.moe/549z46.jpg")]

		for test_data in test_datas:

			new_history_data = UserHistory(operation_name = test_data[0], objectphoto = test_data[1], user = self.test_user)
			new_history_data.save()

	def test_history_links(self):

		
		self.assertEquals(self.test_user.userhistory_set.count(), 4)

		# cek conten benar?
		self.assertEquals(self.test_user.userhistory_set.all()[0].operation_name, "cek item")
		self.assertEquals(self.test_user.userhistory_set.all()[1].operation_name, "compare item")
		self.assertEquals(self.test_user.userhistory_set.all()[2].operation_name, "location check")
		self.assertEquals(self.test_user.userhistory_set.all()[3].operation_name, "about item")

	def test_history_displays(self):

		response = self.client.get('/profile/history/')

		# cek isinya diposting
		self.assertContains(response, "https://files.catbox.moe/rejga0.jpg", count=1, html=False)
		self.assertContains(response, "https://files.catbox.moe/4vdm8q.jpg", count=2, html=False)
		self.assertContains(response, "https://files.catbox.moe/549z46.jpg", count=1, html=False)

class ProfileUnitTests(TestCase):

	def setUp(self):

		# jika akun sudah ada, hapus dulu
		if User.objects.filter(username="test_user").count() == 1:
			User.objects.remove(username="test_user")

		# global display
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-gpu')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

	def tearDown(self):

		self.display.stop()
		self.browser.quit()
	
	def test_register_login_logout(self):

		self.browser.get('localhost:8000')
		time.sleep(1)

		# cek nama terdaftar?
		username_header = self.browser.find_element_by_id("username-header")
		self.assertEquals(username_header.text, "Guest")

		auth_button = self.browser.find_element_by_id("auth_button")
		auth_button.click()
		time.sleep(1)

		# coba daftar
		reg_button = self.browser.find_element_by_id("register_button")
		reg_button.click()
		time.sleep(1)

		# daftar
		username_form = self.browser.find_element_by_id("id_username")
		username_form.send_keys("test_user")
		time.sleep(0.5)

		pass1_form = self.browser.find_element_by_id("id_password1")
		pass1_form.send_keys("chibakutensei123")
		time.sleep(0.5)

		pass2_form = self.browser.find_element_by_id("id_password2")
		pass2_form.send_keys("chibakutensei123")
		time.sleep(0.5)

		reg_button = self.browser.find_element_by_id("register_button")
		reg_button.click()
		time.sleep(1)

		# coba login
		auth_button = self.browser.find_element_by_id("auth_button")
		auth_button.click()
		time.sleep(1)

		username_form = self.browser.find_element_by_id("id_username")
		username_form.send_keys("test_user")
		time.sleep(0.5)

		username_form = self.browser.find_element_by_id("id_password")
		username_form.send_keys("chibakutensei123")
		time.sleep(0.5)

		login_button = self.browser.find_element_by_id("login_button")
		login_button.click()
		time.sleep(1)

		# cek nama terdaftar?
		username_header = self.browser.find_element_by_id("username-header")
		self.assertEquals(username_header.text, "test_user")

		# logout
		auth_button = self.browser.find_element_by_id("auth_button")
		auth_button.click()
		time.sleep(1)

		# kembali ke guest
		username_header = self.browser.find_element_by_id("username-header")
		self.assertEquals(username_header.text, "Guest")
