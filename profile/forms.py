from django import forms
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

class CustomAuthForm(AuthenticationForm):
    attrs = {
        'class':'form-control'
    }

    username = forms.CharField(widget=forms.TextInput(attrs=attrs))
    password = forms.CharField(widget=forms.PasswordInput(attrs=attrs))

class CustomRegistrationForm(UserCreationForm):

	attrs = {
		'class':'form-control'
	}

	username = forms.CharField(max_length=12, widget=forms.TextInput(attrs=attrs))
	password1 = forms.CharField(widget=forms.PasswordInput(attrs=attrs))
	password2 = forms.CharField(widget=forms.PasswordInput(attrs=attrs))