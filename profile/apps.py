from django.apps import AppConfig


class LoginPageConfig(AppConfig):
    name = 'profile_page'
