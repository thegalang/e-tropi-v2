from django.contrib import admin 
from . import views
from django.urls import path, include
from django.contrib.auth import views as auth_views
from profile.forms import CustomAuthForm

app_name = "profile"

urlpatterns = [
	
    path('login/', auth_views.LoginView.as_view(template_name='profile/login.html', authentication_form=CustomAuthForm, 
    											extra_context={"app":"profile"}, redirect_authenticated_user=True), name = 'login'),
    path('logout/', auth_views.LogoutView.as_view(next_page='profile:login'), name = 'logout'),
    path('register/', views.register_user, name="register"),
    path('history/', views.history, name="history"),
]