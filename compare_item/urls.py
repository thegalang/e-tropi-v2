from django.urls import path, include
from compare_item import views

app_name="compare_item"

urlpatterns = [
    path('', views.index, name="home"),
    path('post_data/', views.compare_item, name="compare"),
    path('get_history_data/', views.get_history_data, name="get_history_data"),
    path('get_model_data/', views.get_model_data, name="get_model_data")
]