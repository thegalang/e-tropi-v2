from django.shortcuts import render, reverse, redirect
from .forms import DataForm
from .models import Data, DataSerializer
from django.http import JsonResponse
import random
import json
import time
from django.contrib.auth.models import User
import requests
from PIL import Image
from io import BytesIO
# Create your views here.

def index(request):
    context = {'app':'compare_item'}
    context['form'] = DataForm()
    context['history'] = Data.objects.all()
    return render(request, "compare_item/index.html", context)


# cek apakah link image valid
# sumber: https://stackoverflow.com/questions/11261210/django-check-if-an-image-exists-at-some-particular-url
def valid_image_link(link):
    try:
        response = requests.get(link)
        img = Image.open(BytesIO(response.content))
        img.verify()
        return True
    except:
        return False

def compare_item(request):
    if(request.is_ajax() and request.method == "POST"):
        json_data = json.loads(request.body)
        objectphoto = json_data['data']
        print(objectphoto)
        # ngecek kevalalidan link image
        if(not valid_image_link(objectphoto)):
            response = JsonResponse({'message': "Invalid image link!"})
            response.status_code = 400
            return response
        
        genuine = True
        list_brand = ["Gucci", "H&M", "Hermes", "Uniqlo", "Zara", "Adidas", "Chanel", "Burberry", "Prada", "Levi's"]
        list_location = ["Singapore", "America", "Canada", "Germany"]
        release_date = random.randrange(1999,2019)
        price = random.randrange(150,2000)
        brand = list_brand[random.randrange(9)]
        location = list_location[random.randrange(4)]
        num = random.random()
        print(num)
        if(num>0.5):
            genuine = False

        data_item = Data(Genuine=genuine, objectphoto=json_data['data'], Brand=brand,Release_date=release_date,Price=price,Location=location)
        data_item.save()

        # simpan data ini ke history jika usernya login
        if request.user.is_authenticated:
                current_user = User.objects.get(username=request.user.username)
                new_history = current_user.userhistory_set.create(operation_name="Compare Item", objectphoto = json_data['data'])

        serializer = DataSerializer(data_item)
        serialized = serializer.data

        time.sleep(2)
        
        return JsonResponse(serialized, safe=False)

def get_history_data(request):
    if request.is_ajax():
    
        serializer = DataSerializer(Data.objects.all(), many=True)
        serialized = serializer.data

        time.sleep(2)

        return JsonResponse(serialized, safe=False)

# tidak dihitung di coverage karena sudah di functional test
def get_model_data(request):

    if request.is_ajax():
        obj_id = request.GET.get('obj_id', '')
        
        # cari model dengan id obj_id
    if(obj_id!=''):

        queried = Data.objects.filter(id=obj_id)
        serializer = DataSerializer(queried, many=True)
        time.sleep(2)

        return JsonResponse(serializer.data, safe=False)