from django.apps import AppConfig


class CompareItemConfig(AppConfig):
    name = 'compare_item'
