from django.test import TestCase, Client
from .models import Data
import json
from .views import compare_item
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from pyvirtualdisplay import Display
from django.contrib import auth
from django.contrib.auth.models import User

class CompareItemUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def test_exist_page(self):
        response = self.client.get('/compare_item/')
        self.assertEquals(response.status_code, 200)


    def test_model_saving_works(self):

        new_data = Data(objectphoto = self.test_img_link, Genuine = True , Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")

        self.assertEquals(Data.objects.count(), 0)

        new_data.save()

        self.assertEquals(Data.objects.count(), 1)
        self.assertIsInstance(new_data, Data)

    def test_model_saving_last_24(self):

        for i in range(30):
            new_data = Data(objectphoto = self.test_img_link, Genuine = True,  Brand="Gucci",Release_date=2001,Price=280,Location="Singapore")
            new_data.save()

        self.assertEquals(Data.objects.count(), 24)

        # 6 - 29 ada, 0 - 5 gaada
        self.assertEquals(Data.objects.filter(id=30).count(), 1)
        self.assertEquals(Data.objects.filter(id=7).count(), 1)
        self.assertEquals(Data.objects.filter(id=6).count(), 0)


    def test_random_assignment_works(self):

        genuines = set({})

        # post banyak kali. diharapkan ada yang asli dan ada yang palsu
        for i in range(6):
            response = self.client.post('/compare_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            genuines.add(Data.objects.all()[i].Genuine)

        self.assertEquals(len(genuines), 2)

    def test_history_is_given(self):

        # post banyak untuk ngisi history
        for i in range(3):
            response = self.client.post('/compare_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        
        response = self.client.get('/compare_item/get_history_data/', content_type='application/json', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        json_data = response.json()

        self.assertEquals(len(json_data), 3)


    def test_form_saving_works(self):

        self.assertEquals(Data.objects.count(), 0)
        response = self.client.post('/compare_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        time.sleep(4)
        self.assertEquals(Data.objects.count(), 1)


    def test_query_model_works(self):

        test_cases = ["https://files.catbox.moe/rejga0.jpg",
                     "https://files.catbox.moe/4vdm8q.jpg"]

        # mengisi database data
        for test_case in test_cases:
            response = self.client.post('/compare_item/post_data/',
                                    data={'data': test_case},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # coba query tiap data dan cek apakah sama
        for id, test_case in enumerate(test_cases):
            response = self.client.get('/compare_item/get_model_data/?obj_id='+str(id+1),
                                       content_type='application/json',
                                       HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            json_data = response.json()[0]
            self.assertEquals(json_data["objectphoto"], test_case)

    def test_sending_invalid_link_rejects(self):

        self.assertEquals(Data.objects.count(), 0)

        test_cases = ["https://catbox.moe",
                      "https://www.facebook.com",
                      "https://codeforces.com"]

        for test_case in test_cases:
            response = self.client.post('/compare_item/post_data/',
                                    data={'data': test_case},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(Data.objects.count(), 0)

class CompareItemFunctionalTest(TestCase):

    # set-up and teardown from my story-6
    def setUp(self):

        # global display
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        service_log_path='./chromedriver.log'
        service_args=['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def tearDown(self):

        self.display.stop()
        self.browser.quit()

    def test_submit_displays_label(self):

        self.browser.get('localhost:8000/compare_item/')
        time.sleep(1)

        form_field = self.browser.find_element_by_id('id_ObjectPhoto')
        form_field.send_keys(self.test_img_link)

        time.sleep(1)

        submit_button = self.browser.find_element_by_id('submit_button')

        # cek dulu apakah status tidak ada dan menjadi ada setelah submit
        label_status = self.browser.find_element_by_id('genuine_desc')
        self.assertFalse(label_status.is_displayed())
        submit_button.click()
        time.sleep(20)
        #self.assertIn("The item is", self.browser.page_source)
        self.assertTrue(label_status.is_displayed())

    def test_submit_counterfeit_not_displays_details(self):

        self.browser.get('localhost:8000/compare_item/')
        time.sleep(1)

        form_field = self.browser.find_element_by_id('id_ObjectPhoto')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        details_display = self.browser.find_element_by_class_name('details')
        form_field.send_keys(self.test_img_link)
        time.sleep(1.5)

        # submit terus sampai dapat genuine
        while(True):

            submit_button.click()
            time.sleep(3)

            # ulang terus sampai dapat genuine
            if(label_status.text != "Counterfeit"):
                continue

            # item genuine, cek apakah celebrity muncul
            self.assertFalse(details_display.is_displayed())
            time.sleep(0.5)

            return

    def test_loading_works(self):

        self.browser.get('localhost:8000/compare_item/')
        time.sleep(1)

        form_field = self.browser.find_element_by_id('id_ObjectPhoto')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        item_info = self.browser.find_element_by_id('content')
        loading_gif = self.browser.find_element_by_id('loading_content')

        # coba submit
        form_field.send_keys(self.test_img_link)
        time.sleep(0.5)

        # mulai loading
        submit_button.click()
        self.assertTrue(loading_gif.is_displayed())
        self.assertFalse(item_info.is_displayed())

        # loading seharusnya selesai
        time.sleep(15)
        self.assertFalse(loading_gif.is_displayed())
        self.assertTrue(item_info.is_displayed())

    def test_history_is_showing(self):

        self.browser.get('localhost:8000/compare_item/')
        time.sleep(3)

        form_field = self.browser.find_element_by_id('id_ObjectPhoto')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        form_field.send_keys(self.test_img_link)
        time.sleep(1.5)

        # posting 2 benda
        for i in range(2):

            submit_button.click()
            time.sleep(4)

        time.sleep(4)
        # cek apakah ada class "history_photo" karena itu satu-satunya class dengan foto sejarah
        self.assertIn("history_photo", self.browser.page_source)

    def test_history_is_displaying(self):

        self.browser.get('localhost:8000/compare_item/')
        time.sleep(3)
        
        form_field = self.browser.find_element_by_id('id_ObjectPhoto')
        submit_button = self.browser.find_element_by_id('submit_button')
        label_status = self.browser.find_element_by_id('genuine_desc')
        
        img_links = {"https://files.catbox.moe/rejga0.jpg",
                     "https://files.catbox.moe/4vdm8q.jpg",
                     "https://files.catbox.moe/549z46.jpg"}

        
        # posting 3 benda
        for link in img_links:

            form_field.clear()
            form_field.send_keys(link)
            time.sleep(2)
            submit_button.click()
            time.sleep(4)

        time.sleep(4)

        # coba klik satu-satu
        hist_photos = self.browser.find_elements_by_class_name('history_photo')[-3:]
        found_links = set({})
        for photos in hist_photos:

            time.sleep(1)
            photos.click()
            time.sleep(4)
            found_links.add(self.browser.find_element_by_id('img2').get_attribute('src'))

        # kalau fotonya beda semua, maka history bekerja
        self.assertEquals(len(found_links), 3)



class CekItemAuthUnitTest(TestCase):

    def setUp(self):
        response = self.client.post('/profile/register/', data={"username" : "galatea", 
                                                           "password1" : "orcust_babel",
                                                           "password2" : "orcust_babel"})
        self.test_user = User.objects.get(username="galatea")

        response = self.client.post('/profile/login/', data = {"username" : "galatea",
                                                               "password" : "orcust_babel"}, follow=True)
        # cek sudah kelogin belum
        user = auth.get_user(self.client)
        self.assertTrue(user.is_authenticated)

        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    # cek apakah history dia sudah ada
    def test_posting_data_registered_in_history(self):

        self.assertEquals(self.test_user.userhistory_set.count(), 0)

        request = self.client.post('/compare_item/post_data/',
                                    data={'data': self.test_img_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        # cek apakah terdaftar
        self.assertEquals(self.test_user.userhistory_set.count(), 1)
        self.assertEquals(self.test_user.userhistory_set.all()[0].objectphoto, self.test_img_link)