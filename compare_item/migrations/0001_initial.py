# Generated by Django 2.2.5 on 2019-12-06 11:58

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Data',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('objectphoto', models.TextField(default='')),
                ('Genuine', models.BooleanField(default='')),
                ('Brand', models.CharField(max_length=15)),
                ('Price', models.PositiveIntegerField()),
                ('Release_date', models.PositiveIntegerField()),
                ('Location', models.CharField(max_length=20)),
            ],
        ),
    ]
