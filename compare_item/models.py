from django.db import models
from rest_framework import serializers

# Create your models here.
def save_last_24(AnyModel):
    recents = AnyModel.objects.all()
    if(len(recents)>24):
        last_24 = AnyModel.objects.all()[len(recents) - 24:]
        AnyModel.objects.exclude(id__in=last_24).delete()

class Data(models.Model):
    objectphoto = models.TextField(default = "")
    Genuine = models.BooleanField(default="")
    Brand = models.CharField(max_length=15)
    Price = models.PositiveIntegerField()
    Release_date = models.PositiveIntegerField()
    Location = models.CharField(max_length=20)

    class meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)
        save_last_24(Data)

# class untuk serialize secara rekursif. 
# sumber: https://ruddra.com/posts/django-serialize-foreignkey-m2m-property-instance-fields/ 
class DataSerializer(serializers.ModelSerializer):

    class Meta:
        model = Data
        fields = "__all__"
        depth = 1
        