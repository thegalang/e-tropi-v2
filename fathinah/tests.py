from django.test import TestCase, Client
from django.urls import resolve
from . import views
from .models import Map, Data
from PIL import Image
from io import BytesIO
from django.core.files.base import File
import json
from selenium import webdriver
import time
from datetime import datetime
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
from pyvirtualdisplay import Display
from django.contrib import auth
from django.contrib.auth.models import User
import json
from django.http import HttpRequest


class CekItemUnitTest(TestCase):

    def setUp(self):
        self.client = Client()
        self.test_link = "https://files.catbox.moe/66dyhl.png"

    def test_page_exist(self):
        response = Client().get('/cek_peta/')
        self.assertEqual(response.status_code, 200)

    def test_views_connected_to_page(self):
        found = resolve("/cek_peta/")
        self.assertEqual(found.func, views.index)

    def test_page_display_contains_words(self):
        request = HttpRequest()
        response = views.index(request)
        self.assertContains(response, "Image link of item you want to check")

    def test_model_saving_works(self):
        test_map = Map(map="Indonesia", city="Garut", cityPict="https://files.catbox.moe/jvrjyx.jpg",
                       mapPict="https://files.catbox.moe/px68pe.png", id=1)
        test_map.save()
        new_data = Data(objectphoto=self.test_link, map=test_map)

        self.assertEqual(Data.objects.count(), 0)
        new_data.save()

        self.assertEqual(Data.objects.count(), 1)
        self.assertIsInstance(new_data, Data)

    def test_form_saving_works(self):
        self.assertEquals(Data.objects.count(), 0)
        test_map = Map(cityPict="https://files.catbox.moe/jvrjyx.jpg", mapPict="https://files.catbox.moe/px68pe.png",
                       city="Depok", map="Indonesia", id=1)
        test_map.save()
        response = self.client.post('/cek_peta/post_data/',
                                    data={'data': self.test_link},
                                    content_type='application/json',
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEquals(Data.objects.count(), 1)


class CekItemModelUnitTest(TestCase):
    def setUp(self):
        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def test_history_is_given(self):
        test_map = Map(cityPict="https://files.catbox.moe/jvrjyx.jpg", mapPict="https://files.catbox.moe/px68pe.png",
                       city="Depok", map="Indonesia", id=1)
        test_map.save()

        # post banyak untuk ngisi history
        for i in range(3):
            response = self.client.post('/cek_peta/post_data/',
                                        data={'data': self.test_img_link},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        response = self.client.get('/cek_peta/get_history_data/',
                                   content_type='application/json',
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        json_data = response.json()

        self.assertEquals(len(json_data), 3)

    def test_query_model_works(self):
        test_map = Map(cityPict="https://files.catbox.moe/jvrjyx.jpg", mapPict="https://files.catbox.moe/px68pe.png",
                       city="Depok", map="Indonesia", id=1)
        test_map.save()

        test_cases = ["https://files.catbox.moe/rejga0.jpg",
                      "https://files.catbox.moe/4vdm8q.jpg"]

        # mengisi database data
        for test_case in test_cases:
            response = self.client.post('/cek_peta/post_data/',
                                        data={'data': test_case},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        # coba query tiap data dan cek apakah sama
        for id, test_case in enumerate(test_cases):
            response = self.client.get('/cek_peta/get_model_data/?obj_id=' + str(id + 1),
                                       content_type='application/json',
                                       HTTP_X_REQUESTED_WITH='XMLHttpRequest')
            json_data = response.json()[0]
            self.assertEquals(json_data["objectphoto"], test_case)

    def test_sending_invalid_link_rejects(self):
        self.assertEquals(Data.objects.count(), 0)

        test_cases = ["https://catbox.moe",
                      "https://www.facebook.com",
                      "https://codeforces.com"]

        test_map = Map(cityPict="https://files.catbox.moe/jvrjyx.jpg", mapPict="https://files.catbox.moe/px68pe.png",
                       city="Depok", map="Indonesia", id=1)
        test_map.save()

        for test_case in test_cases:
            response = self.client.post('/cek_peta/post_data/',
                                        data={'data': test_case},
                                        content_type='application/json',
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.assertEquals(Data.objects.count(), 0)


class CekPetaFunctionalTest(TestCase):

    def setUp(self):
        # global display
        self.display = Display(visible=0, size=(800, 600))
        self.display.start()
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-gpu')
        service_log_path = './chromedriver.log'
        service_args = ['--verbose']
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

        self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

    def tearDown(self):
        self.display.stop()
        self.browser.quit()

    # ngecek apakah genuine display celeb, counterfeit tidak display celeb
    def test_peta_is_displaying(self):
        self.browser.get('localhost:8000/cek_peta/')
        time.sleep(1)

        form_field = self.browser.find_element_by_id('id_data')
        submit_button = self.browser.find_element_by_id('button')
        display = self.browser.find_element_by_id('content')
        form_field.send_keys(self.test_img_link)
        time.sleep(1.5)
        submit_button.click()
        time.sleep(6)

        self.assertTrue(display.is_displayed())

    # test apakah history dapat di klik. sekalin test loading
    def test_history_is_displaying(self):
        self.browser.get('localhost:8000/cek_peta/')
        time.sleep(3)

        form_field = self.browser.find_element_by_id('id_data')
        submit_button = self.browser.find_element_by_id('button')
        loading_gif = self.browser.find_element_by_id('loading_content')
        img_links = {"https://files.catbox.moe/rejga0.jpg"}

        for link in img_links:
            form_field.clear()
            form_field.send_keys(link)

            time.sleep(2)
            submit_button.click()
            time.sleep(0.1)
            self.assertTrue(loading_gif.is_displayed())
            time.sleep(6)
            self.assertFalse(loading_gif.is_displayed())

        time.sleep(5)

        # coba klik satu-satu
        hist_photos = self.browser.find_elements_by_id('history_content')[-1:]
        found_links = set({})
        for photos in hist_photos:
            time.sleep(2)
            photos.click()
            time.sleep(6)
            found_links.add(self.browser.find_element_by_class_name('history_photo').get_attribute('src'))

        # kalau fotonya beda semua, maka history bekerja
        self.assertEquals(len(found_links), 1)
