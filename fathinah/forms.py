from django import forms


class DataForm(forms.Form):
    data = forms.CharField(label="Image link of item you want to check")
