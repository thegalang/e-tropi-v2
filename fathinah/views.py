from django.shortcuts import render, redirect
from .models import Data, DataSerializer, Map
from .forms import DataForm
from django.http import JsonResponse
import random
import json
import time
from django.contrib.auth.models import User
import requests
from PIL import Image
from io import BytesIO


def selectRandomMap(data):

    selected = Map.objects.order_by('?')[0]
    data.map = selected


def post_data(request):
    if request.method == "POST":
        json_data = json.loads(request.body)

        # ngecek kevalalidan link image
        if (not valid_image_link(json_data["data"])):
            response = JsonResponse({'message': "Invalid image link!"})
            response.status_code = 400
            return response

        data = Data(objectphoto=json_data['data'])
        selectRandomMap(data)
        data.save()

        if request.user.is_authenticated: # pragma: no cover
            current_user = User.objects.get(username=request.user.username)
            new_history = current_user.userhistory_set.create(operation_name="Location Check",
                                                              objectphoto=json_data['data'])

        # ubah data ini ke json lalu balikin
        serializer = DataSerializer(data)
        # serialized = serializer.serialize('json', [data])
        serialized = serializer.data

        return JsonResponse(serialized, safe=False)


def index(request):
    context = {'app': 'cek_peta'}
    form = DataForm()
    context['form'] = form
    return render(request, 'fathinah/peta.html', context)


def get_history_data(request):
    if request.is_ajax():
        serializer = DataSerializer(Data.objects.all(), many=True)
        serialized = serializer.data

        time.sleep(2)

        return JsonResponse(serialized, safe=False)


def get_model_data(request):
    if request.is_ajax():
        obj_id = request.GET.get('obj_id', '')

        # cari model dengan id obj_id
        if (obj_id != ''):
            queried = Data.objects.filter(id=obj_id)
            serializer = DataSerializer(queried, many=True)
            time.sleep(2)

            return JsonResponse(serializer.data, safe=False)


def valid_image_link(link):
    try:

        response = requests.get(link)
        img = Image.open(BytesIO(response.content))
        img.verify()
        return True

    except:
        return False
