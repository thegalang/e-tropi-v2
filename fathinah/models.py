from django.db import models
from rest_framework import serializers


# hanya menyimpan 24 elemen terakhir
def keep_last_24(MyModel): # pragma: no cover
    most_recents = MyModel.objects.all()

    if (len(most_recents) > 24):
        last24 = MyModel.objects.all()[len(most_recents) - 24:]
        MyModel.objects.exclude(id__in=last24).delete()


class Map(models.Model):
    map = models.TextField(max_length=20, default="")
    city = models.TextField(max_length=20, default="")

    mapPict = models.TextField()
    cityPict = models.TextField()


class Data(models.Model):
    objectphoto = models.TextField(default="")
    map = models.ForeignKey(Map, on_delete=models.CASCADE)

    class meta:
        ordering = ['id']

    def save(self, *args, **kwargs):
        super(Data, self).save(*args, **kwargs)

        keep_last_24(Data)


# class untuk serialize secara rekursif.
# sumber: https://ruddra.com/posts/django-serialize-foreignkey-m2m-property-instance-fields/

class DataSerializer(serializers.ModelSerializer):
    class Meta:
        model = Data
        fields = "__all__"
        depth = 1
