from .forms import UploadForm
from .models import UploadModel, DataSerializer
from django.contrib.auth.models import User
from django.http import JsonResponse
from django.shortcuts import render, redirect

import random
import json
import time
import requests
from PIL import Image
from io import BytesIO

material_selection = ["Leather", "Cotton", "Nylon", "Canvas", "Straw", "Denim", "Neoprene", "Mesh"]

year_selection = [str(x) for x in range(2010, 2020)]

# cek apakah link image valid
# sumber: https://stackoverflow.com/questions/11261210/django-check-if-an-image-exists-at-some-particular-url
def valid_image_link(link):

	try:

		response = requests.get(link)
		img = Image.open(BytesIO(response.content))
		img.verify()
		return True

	except:
		return False

def post_data(request): 
	if request.is_ajax() and request.method == 'POST':
		json_data = json.loads(request.body)
		objectphoto = json_data['data']

		# ngecek kevalalidan link image
		if(not valid_image_link(objectphoto)):
			response = JsonResponse({'message': "Invalid image link!"})
			response.status_code = 400
			return response

		data = UploadModel(objectphoto = json_data['data'],
						   material = random.sample(material_selection, 1)[0],
						   price = random.randint(100000, 10000000),
						   year = random.sample(year_selection, 1)[0])
		
		data.save()

		# simpan data ini ke history jika usernya login
		if request.user.is_authenticated:
			current_user = User.objects.get(username=request.user.username)
			new_history = current_user.userhistory_set.create(operation_name="About Item", 
															  objectphoto = json_data['data'])

		# ubah data ini ke json lalu balikin
		serializer = DataSerializer(data)
		serialized = serializer.data

		return JsonResponse(serialized, safe=False)

def get_history_data(request): 
	if request.is_ajax():

		serializer = DataSerializer(UploadModel.objects.all(), many=True)
		serialized = serializer.data

		time.sleep(2)

		return JsonResponse(serialized, safe=False)


def get_model_data(request): 
	if request.is_ajax():
		obj_id = request.GET.get('obj_id', '')
		
		# cari model dengan id obj_id
		if(obj_id!=''):

			queried = UploadModel.objects.filter(id=obj_id)
			serializer = DataSerializer(queried, many=True)
			time.sleep(2)

			return JsonResponse(serializer.data, safe=False)

def index(request):
	context = {'app' : 'about_item'}
	form  = UploadForm()
	context['form'] = form
	return render(request, 'about_item/index.html', context)