from django import forms

class UploadForm(forms.Form):
	
	data = forms.CharField(label="Image link of item you want to check")