from django.db import models
from rest_framework import serializers

def keep_last_24(MyModel):
	most_recents = MyModel.objects.all()
	if(len(most_recents)>24):
		last24 = MyModel.objects.all()[len(most_recents) - 24:]
		MyModel.objects.exclude(id__in=last24).delete()

class UploadModel(models.Model):
	
	objectphoto = models.TextField(default = "")
	material = models.CharField(max_length = 15)
	price = models.PositiveIntegerField()
	year = models.CharField(max_length = 6)

	class meta:
		ordering = ['id']

	def save(self, *args, **kwargs):

		super(UploadModel, self).save(*args, **kwargs)

		keep_last_24(UploadModel)

# class untuk serialize secara rekursif. 
# sumber: https://ruddra.com/posts/django-serialize-foreignkey-m2m-property-instance-fields/ 
class DataSerializer(serializers.ModelSerializer):
	class Meta:
		model = UploadModel
		fields = "__all__"
		depth = 1