from .models import UploadModel
from .views import post_data
from django.test import TestCase, Client
from django.urls import resolve
from django.contrib import auth
from django.contrib.auth.models import User
from pyvirtualdisplay import Display
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver import DesiredCapabilities
import json
import time

class AboutItemUnitTest(TestCase):
	def setUp(self):
		self.client = Client()
		self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

	def test_exist_page(self):
		response = self.client.get('/about_item/')
		self.assertEquals(response.status_code, 200)

	def test_model_saving_works(self):
		test_data = UploadModel(objectphoto = self.test_img_link, material = "cotton", price="1000", year = 2015)
		self.assertEquals(UploadModel.objects.count(), 0)

		test_data.save()

		self.assertEqual(UploadModel.objects.count(),1)			
		self.assertIsInstance(test_data, UploadModel)

	def test_model_saving_last_24(self):
		for id_obj in range(30):
			test_data = UploadModel(objectphoto = self.test_img_link,  material = "cotton", price="1000", year = 2015)
			test_data.save()

		self.assertEquals(UploadModel.objects.count(), 24)

		self.assertEquals(UploadModel.objects.filter(id=30).count(), 1)
		self.assertEquals(UploadModel.objects.filter(id=7).count(), 1)
		self.assertEquals(UploadModel.objects.filter(id=6).count(), 0)

	def test_form_saving_works(self):
		self.assertEquals(UploadModel.objects.count(), 0)
		response = self.client.post('/about_item/post_data/', 
									data = {'data' : self.test_img_link},
									content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

		self.assertEquals(UploadModel.objects.count(), 1)

	def test_sending_invalid_link_rejects(self):

		self.assertEquals(UploadModel.objects.count(), 0)

		test_cases = ["https://catbox.moe",
					  "https://www.facebook.com"]

		for test_case in test_cases:
			response = self.client.post('/about_item/post_data/',
								    data={'data': test_case},
								    content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

		self.assertEquals(UploadModel.objects.count(), 0)

# test yang berhubungan dengan models
class AboutItemModelUnitTest(TestCase):
	def setUp(self):
		self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

	def test_history_is_given(self):
		# post banyak untuk ngisi history
		for i in range(3):
			response = self.client.post('/about_item/post_data/',
								    data={'data': self.test_img_link},
								    content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
		
		response = self.client.get('/about_item/get_history_data/',
									content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
		json_data = response.json()

		self.assertEquals(len(json_data), 3)

	def test_query_model_works(self):
		test_cases = ["https://files.catbox.moe/rejga0.jpg",
					 "https://files.catbox.moe/4vdm8q.jpg"]

		# mengisi database data
		for test_case in test_cases:
			response = self.client.post('/about_item/post_data/',
								    data={'data': test_case},
								    content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')

		# coba query tiap data dan cek apakah sama
		for id, test_case in enumerate(test_cases):
			response = self.client.get('/about_item/get_model_data/?obj_id='+str(id+1),
									   content_type='application/json',
								       HTTP_X_REQUESTED_WITH='XMLHttpRequest')
			json_data = response.json()[0]
			self.assertEquals(json_data["objectphoto"], test_case)

# mengetest berhubungan dengan authentication di about item
class AboutItemAuthUnitTest(TestCase):
	def setUp(self):
		response = self.client.post('/profile/register/', data={"username" : "test", 
														   "password1" : "test_login_123",
														   "password2" : "test_login_123"})
		self.test_user = User.objects.get(username="test")

		response = self.client.post('/profile/login/', data = {"username" : "test",
															   "password" : "test_login_123"}, follow=True)
		
		# cek sudah kelogin belum
		user = auth.get_user(self.client)
		self.assertTrue(user.is_authenticated)

		self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

	# cek apakah history dia sudah ada
	def test_posting_data_registered_in_history(self):
		self.assertEquals(self.test_user.userhistory_set.count(), 0)

		request = self.client.post('/about_item/post_data/',
								    data={'data': self.test_img_link},
								    content_type='application/json',
								    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
		# cek apakah terdaftar
		self.assertEquals(self.test_user.userhistory_set.count(), 1)
		self.assertEquals(self.test_user.userhistory_set.all()[0].objectphoto, self.test_img_link)
		
class AboutItemFunctionalTest(TestCase):
	def setUp(self):
		# global display
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--disable-gpu')
		service_log_path='./chromedriver.log'
		service_args=['--verbose']
		self.browser = webdriver.Chrome('./chromedriver', chrome_options = chrome_options)

		self.test_img_link = "https://files.catbox.moe/pqdxzy.jpg"

	def tearDown(self):
		self.display.stop()
		self.browser.quit()

	# test apakah history dapat di klik
	def test_history_is_displaying(self):
		self.browser.get('localhost:8000')
		time.sleep(3)
		
		form_field = self.browser.find_element_by_id('id_data')
		submit_button = self.browser.find_element_by_id('submit_button')
		label_status = self.browser.find_element_by_id('genuine_desc')
		loading_gif = self.browser.find_element_by_id('loading_content')
		img_links = {"https://files.catbox.moe/rejga0.jpg",
					 "https://files.catbox.moe/4vdm8q.jpg"}

		# posting 2 benda
		for link in img_links:
			form_field.clear()
			form_field.send_keys(link)

			time.sleep(2)
			submit_button.click()
			self.assertTrue(loading_gif.is_displayed())
			time.sleep(4)
			self.assertFalse(loading_gif.is_displayed())

		time.sleep(4)

		# coba klik satu-satu
		hist_photos = self.browser.find_elements_by_class_name('history_photo')[-2:]
		found_links = set({})
		for photos in hist_photos:

			time.sleep(1)
			photos.click()
			time.sleep(4)
			found_links.add(self.browser.find_element_by_id('image').get_attribute('src'))

		# kalau fotonya beda semua, maka history bekerja
		self.assertEquals(len(found_links), 2)