from django.apps import AppConfig

class AboutItemConfig(AppConfig):
    name = 'about_item'
