[![pipeline status](https://gitlab.com/thegalang/e-tropi-v2/badges/master/pipeline.svg)](https://gitlab.com/thegalang/e-tropi-v2/commits/master)
[![coverage report](https://gitlab.com/thegalang/e-tropi-v2/badges/master/coverage.svg)](https://gitlab.com/thegalang/e-tropi-v2/commits/master)

# e-tropi
Kelompok: KB03

Nama-nama anggota kelompok:
1. Fathinah Asma Izzati
2. Galangkangin Gotera
3. Muhammad Zuhdi Zamrud
4. Salsabila Adnan

## Deskripsi:
Membuat website sebagai pusat informasi untuk barang-barang asli dan KW. Pada website kami, kita bisa mengetahui berbagai jenis informasi mengenai barang-barang KW. Seperti negara asalnya, indikator-indikator KWnya, dan perbandingannya dengan barang asli.

## Manfaat:
Kami berharap dengan ide dari website ini, industri konsumen dapat menjadi lebih positif. Produsen barang-barang akan lebih cermat dalam membuat barang-barangnya sehingga tidak cacat dan diklasifikasikan KW, dan juga penipuan yang disebabkan oleh barang-barang KW dapat dikurangi.

## Daftar fitur yang akan diimplementasikan:

### 1. Mengecek keaslian barang (Check Item): Galangkangin Gotera

Pada laman ini, user dapat mengupload foto. Lalu, akan ditampilkan apakah barang tersebut asli atau kw. Jika barang tersebut asli, laman ini juga akan menampilkan artis-artis yang juga memakai barang tersebut. Jika foto artis di klik, google search nama artis tersebut akan dilakukan. Diharapkan ini meningkatkan motivasi pemakai dalam memakai barangnya.

### 2. Mengecek negara asal barang tersebut (Location Check): Fathinah Asma Izzati

Pada laman ini, user dapat mengupload foto. Lalu, baik asli maupun palsu, akan ditampilkan daerah asal dari barang tersebut. Daerah ini berbentuk peta yang dimana provinsinya akan ditandai lalu diberikan foto satelit dari daerah produksinya. 

Misal, apabila sebuah barang KW berasal dari Beijing, China, Akan ada dua buah gambar peta. Satu gambar menandai provinsi beijing pada peta. Satu gambar lagi memberikan foto satelit dari gambar tersebut tentang letak daerah tersebut.

### 3 Mengecek indikator-indikator KW barang tersebut (About Item): Salsabila Adnan

Pada fitur ini, user dapat mengupload foto. Lalu, apabila barang tersebut palsu, akan ditampilkan apa saja indikator-indikator yang membuat barang tersebut palsu. Misalnya, barang dapat dikatakan palsu apabila tidak memiliki logo mereknya, sisi lebih melengkung dari produk asli, ataupun teksturnya tidak sesuai dengan aslinya.

### 4. Membandingkan barang tersebut dengan barang aslinya (Compare Item): Zuhdi Zamrud

Pada fitur ini, user dapat mengupload foto. Jika barang tersebut palsu, akan ditampilkan barang aslinya sebagai cara untuk membandingkan dua barang tersebut. Kami tujukan fitur ini untuk para kurator muda yang ingin belajar caranya membedakan barang asli dan palsu.

Kami juga menyediakan fitur “history” untuk setiap laman. Pada setiap laman, user dapat melihat foto-foto apa saja yang sudah pernah di kirim oleh user-user lain. User dapat mengklik foto tersebut dan akan menampilkan informasi yang sesuai untuk foto tersebut.

## Fitur Tambahan

### 1. History

Setiap halaman submit memiliki history foto barang yang pernah disubmit pada bagiannya. Klik foto tersebut untuk kembali buka hasil untuk barang tersebut. Barang yang historynya sedang dilihat akan memiliki border berwarna hijau (menggunakan jQuery)

### 2. Accounts

User dapat membuat akun dengan klik login -> register. Saat register, user diminta memasukkan username dan password. Password akan divalidasi 2x dan dicek apakah kedua password sama dan apakah password cukup kuat. Jika tidak, akan ada error yang dikembalikan.

Dengan akun, user dapat mengakses fitur "user history" dengan klik usernamenya pada navbar. Ini akan membawa user ke halaman berisi tabel 20 barang terakhir yang pernah dicari tahunya beserta servis apa yang dipakai (compare item, location check, about item, check item).

### 3. Webservice & AJAX

Seluruh servis pencarian website kami menggunakan AJAX untuk mengirim dan menyimpan data. Ketika user mengupload link, user akan disuguhkan loading screen sampai komputasi informasi barangnya selesai dilakukan. Ini memungkinkan user tetap mengakses websitenya secara asyncronous. AJAX mengirim JSON kedalam server django dan menerima JSON dari server django juga. 

### 4. Form Link Validation

Form kami memiliki link validation. Jadi ketika user memasukkan link yang **tidak** mengarah ke foto, website akan menolak link tersebut dan memberi alert. Ini sebagai proteksi website kami dari spam link-link yang mungkin berisi hal-hal yang tidak diinginkan.